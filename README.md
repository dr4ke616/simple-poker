#Simple Poker

##Compile and Run

- To compile the program just open up the directory in command prompt where the
SimplePoker.java, images and pokerpkg folders are located
- To compile the program type "javac" and the java file name i.e. "javac SimplePoker.java"
into command prompt.
- After its compiled in order to run it just type java then the file name
(not including an extension) i.e. "java SimplePoker"
- If you get an error it may be due to the class path, to set the correct class path type
"set classpath=.;" into the command line then press enter, then try to rerun the program
- All the classes; Card.java, CardSet.java and CardGame.java are within a package
called "pokerpkg"
- The Main class SimplePoker.java is in the parent directory of "pokerpkg".

##Game Play

- When you run the application you are to click "deal" to deal four new cards.
- If the user wished to hold certain cards tick the check boxes under the
cards then click "second deal".
- After that happens if you win the score and total score will be updated and
your winning hand will be displayed.
- The score only displays the score achieved from that one hand.
- The total score displays all the scores achieved as the user plays through the game.
- After you click "new game" from the "game" menu and repeat the same again.
- The user can also clear scores and exit the game from the same menu.
