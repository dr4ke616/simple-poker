/*************************************************************************/
/**	Name: Adam Drakeford												**/
/**	Student ID: C08306176												**/
/**	Class Description:													**/
/**		This is the main class. It calls a new Card Game object			**/
/*************************************************************************/
import pokerpkg.*;
import java.io.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import javax.swing.JMenuBar.*;
import java.awt.image.*;
import javax.imageio.ImageIO;
import java.util.*;

public class SimplePoker
{
	/*	Main creates a new Card Game object
		The IOException that is thrown is for
		the method "loadImage" in the Card class 
		is used incase there is an error while reading
		in the image
	*/
	public static void main(String[] args ) throws IOException
	{
		CardGame sp = new CardGame("Simple Poker");
	}
}//end SimplePoker class