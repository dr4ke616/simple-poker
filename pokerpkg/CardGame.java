/*************************************************************************/
/**	Name: Adam Drakeford												**/
/**	Student ID: C08306176												**/
/**	Class Description:													**/
/**		This class provides the main GUI interface that the				**/
/**		user interacts with.											**/
/*************************************************************************/

package pokerpkg;

import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;
import javax.swing.JMenuBar.*;
import java.awt.image.*;
import javax.imageio.ImageIO;
import java.util.*;

public class CardGame extends JFrame implements ActionListener
{
	//Sets a static variable for the number of cards in a hand
	public static final int NO_OF_CARDS = 4;
	public static final Color backgroundColor = new Color(54, 170, 42);
	//Font object for any text in the interface, initialised in the constructor
	private Font scoreFont;
	private Font winningHandFont;
	//Initialises the CardSet object
	private CardSet hand;
	private Container initalArea;
	//cardArea pannel will be inserted into "initalArea"
	private JPanel cardArea;
	//The button that will allow user to deal for first time
	private JButton newDealButton;
	//The button that will allow user to deal for second time
	private JButton secondDealButton;
	//Array of panels used for temporarily holding panels 
	//that are returned from the "addCard()" method
	private JPanel[] usersCurrentHand;		
	//CheckBox is used in the addCard method 
	//and checked in the actionPerformed method
	private JCheckBox[] checkBox;			
	//newGameFn is a menu item in the menu bar - option creates new game
	private JMenuItem newGameFn;
	//clearScoresFn is a menu item in the menu bar - option clears scores
	private JMenuItem clearScoresFn;
	//exitFn is a menu item in the menu bar - Exites the game
	private JMenuItem exitFn;
	//Three below labels are for the scoring and winning hands
	private Label tsLabel;
	private Label sLabel;
	private Label handLabel;

	/*Constructor takes the title of the game as an argument*/
	public CardGame(String title)
	{
		super(title);
		
		//Initialisisng the class variables
		checkBox = new JCheckBox[NO_OF_CARDS];
		scoreFont = new Font("Arial", Font.BOLD, 15);
		winningHandFont = new Font("Arial", Font.BOLD, 20);
		initalArea = new JPanel();
		cardArea = new JPanel();
		usersCurrentHand = new JPanel[NO_OF_CARDS];
		hand = new CardSet();
		tsLabel = new Label();
		sLabel = new Label();
		handLabel = new Label();
		exitFn = new JMenuItem();
		
		//Setting the size and location of the window
		setSize(800,550);
		setLocation(250,100);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		
		//Calling methods to set the layout and menu bar
		setContentPane(createLayout());
		setJMenuBar(createMenuBar());

		setVisible(true);
	}//end overloaded constructor - CardGame()
	
	/*Method creates the layout for the for the game window*/
	private Container createLayout()
	{
		//Initial Area is the first panel with a 
		//border layout, the background colour is also set here
		initalArea.setLayout(new BorderLayout());
		initalArea.setBackground(backgroundColor);
		
		/*SCORE CONFIGURATION*/
		//The two label variables are inititalised as class variables
		//The scoreArea panel is where the score labels will be added to
		JPanel scoreArea = new JPanel();;
		scoreArea.setLayout(new GridLayout(2,1));
		scoreArea.setBackground(backgroundColor);
		//This is adding text to display the score
		sLabel.setText("Score: "+hand.getScore());
		tsLabel.setText("Total Score: "+hand.getTotalScore());
		tsLabel.setFont(scoreFont);
		sLabel.setFont(scoreFont);
		scoreArea.add(sLabel);
		scoreArea.add(tsLabel);

		initalArea.add(scoreArea, BorderLayout.SOUTH);

		/*CARD CONFIGURATION*/
		//Setting layout for the cardArea variable that was initialised at class level
		//The cardArea panel is configured to take the images of the cards 
		//and add the images to the panel in a method called addCard()
		cardArea.setLayout(new FlowLayout(FlowLayout.CENTER, 70, 110));
		cardArea.setBackground(backgroundColor);

		initalArea.add(cardArea, BorderLayout.CENTER);
		
		/*BUTTON CONFIGURATION*/
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		buttonPanel.setBackground(backgroundColor);
		
		//Adds a spacer for the distance between top of buttons and top of panel
		buttonPanel.add(Box.createRigidArea(new Dimension(0,150)));
		//Adds a spacer for the distance between left of buttons and left of panel
		buttonPanel.add(Box.createRigidArea(new Dimension(150,0)));

		newDealButton = new JButton ("New Deal");
		//Adds the listener to the "New Deal" button
		newDealButton.addActionListener(this);		
		buttonPanel.add(newDealButton);
		//Adds a spacer for the distance between two buttons
		buttonPanel.add(Box.createRigidArea(new Dimension(0,20)));

		secondDealButton = new JButton ("Second Deal");
		//Adds the listener to the "Second Deal" button
		secondDealButton.addActionListener(this);
		//Makes the "secondDealButton" unusable,
		//this will change in the "actionPerformed" method
		secondDealButton.setEnabled(false);
		//Sets allignment for two buttons
		newDealButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		secondDealButton.setAlignmentX(Component.CENTER_ALIGNMENT);		
		buttonPanel.add(secondDealButton);

		initalArea.add(buttonPanel, BorderLayout.WEST);

		/*DISPLAYS USERS BEST HAND*/
		//Label for hand is initialised as class variable
		JPanel handPanel = new JPanel();
		handPanel.setBackground(backgroundColor);
		handLabel.setText("Info: ");
		handLabel.setFont(winningHandFont);
		handPanel.add(handLabel);
		initalArea.add(handPanel, BorderLayout.NORTH);

		return initalArea;
	}//end createLayout()

	/*This method creates the Menu that appears at the top of the window*/
	private JMenuBar createMenuBar()
	{
		JMenuBar menuBar = new JMenuBar();
		JMenu gameMenu = new JMenu("Game");

		newGameFn = new JMenuItem("New Game");
		//adds the action listener
		newGameFn.addActionListener(this);

		clearScoresFn = new JMenuItem("Clear Scores");
		//adds the action listener
		clearScoresFn.addActionListener(this);

		exitFn = new JMenuItem("Exit");
		//adds the action listener
		exitFn.addActionListener(this);

		gameMenu.add(newGameFn);
		gameMenu.add(clearScoresFn);
		gameMenu.add(exitFn);

		menuBar.add(gameMenu);

		return menuBar;
	}//end createMenuBar()	
	
	/*	This method is called four times in the actionPerformed method, 
		for each card the users hand
	*/
	public JPanel addCard(int cardNumber)
	{
		//Grid Layout for the card and check box
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(2,1));

		//The Area the card will be displayed		
		JPanel cardPanel = new JPanel();		
		cardPanel.setBackground(backgroundColor);
		
		//Adding images too game
		JLabel cardLabel = new JLabel();
		
		//Calls deal from CardSet class
		Icon cardIcon = new ImageIcon(hand.deal(cardNumber));
		cardLabel.setIcon(cardIcon);

		cardPanel.add(cardLabel);
		
		//Setting size of pannel image sits in
		cardPanel.setPreferredSize(new Dimension(79, 150));
		panel.add(cardPanel);
		
		//The check box
		JPanel checkBoxPanel = new JPanel();
		checkBoxPanel.setBackground(backgroundColor);
		checkBox[cardNumber] = new JCheckBox("Hold");
		checkBox[cardNumber].setBackground(backgroundColor);
		checkBoxPanel.add(checkBox[cardNumber]);

		panel.add(checkBoxPanel);
		
		//Returns Panel
		return panel;
	}//end addCard()

	/*This method is the event handler method which allows user interaction*/
	public void actionPerformed (ActionEvent e)
	{
		if(e.getSource() instanceof JButton){

			//When first deal button is pressed it creates a new Card object called firstHand
			if(e.getSource() == newDealButton){
				newDealButton.setEnabled(false);
				secondDealButton.setEnabled(true);

				//This adds the four cards the the screen by
				//by cycling through the CardSet object - addCard is a local method
				for(int i=0; i<NO_OF_CARDS; i++){
					usersCurrentHand[i] = addCard(i);
					cardArea.add(usersCurrentHand[i]);
				}

				hand.determineSuit();				//Determines the suit type for each card
													//Calls a method in the CardSet class
				hand.determineValue();				//Determines the value for each card
													//Calls a method in the CardSet class
				//The setVisible() method is called 
				//to refresh the images
				setVisible(true);
			}
			if(e.getSource() == secondDealButton){
				secondDealButton.setEnabled(false);

				//This checks which card the user wishes to hold and updates the 
				//images of the cards to match the users choice

				//The array is used to temporally store the jpanel with the images
				//This is to ensure that the images are displayed correctly

				//The array "usersCurrentHand" is of type JPanel
				for(int k=0; k<NO_OF_CARDS; k++)
					cardArea.remove(usersCurrentHand[k]);
				for(int i=0; i<NO_OF_CARDS; i++){
					if(!checkBox[i].isSelected())
						usersCurrentHand[i] = null;
				}
				for(int j=0; j<NO_OF_CARDS; j++){
					if(usersCurrentHand[j] == null)
						usersCurrentHand[j] = addCard(j);
				}
				for(int l=0; l<NO_OF_CARDS; l++){
					cardArea.add(usersCurrentHand[l]);
					//This disables the use of the textboxes
					checkBox[l].setEnabled(false);
				}

				hand.determineSuit();				//Determines the suit type for each card
													//Calls a method in the CardSet class
				hand.determineValue();				//Determines the value for each card
													//Calls a method in the CardSet class
				//This updates the score each time the
				//second deal button is pressed
				hand.updateScore();
				
				//Updates the scores and winning hand
				tsLabel.setText("Total Score: "+hand.getTotalScore());
				sLabel.setText("Score: "+hand.getScore());
				handLabel.setText("Info: "+hand.getWinningHand());

				//The setVisible() method is called 
				//to refresh the images
				setVisible(true);
			}
		}
		if(e.getSource() instanceof JMenuItem){
			//If the new game button is pressed it reinitialises some of the variables
			if(e.getSource() == newGameFn){
				initalArea = new JPanel();
				cardArea = new JPanel();
				usersCurrentHand = new JPanel[NO_OF_CARDS];
				hand = new CardSet();
				
				setContentPane(createLayout());
				setJMenuBar(createMenuBar());

				setVisible(true);
			}
			//this clears the old scores back to zero
			if(e.getSource() == clearScoresFn){
				hand.clearScores();
				tsLabel.setText("Total Score: "+hand.getTotalScore());
				sLabel.setText("Score: "+hand.getScore());
				setVisible(true);
			}
			//this exits the game
			if(e.getSource() == exitFn){
				System.exit(0);
			}
		}
	}//end actionPerformed()
}//CardGame class