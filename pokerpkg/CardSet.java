/*************************************************************************/
/**	Name: Adam Drakeford												**/
/**	Student ID: C08306176												**/
/**	Class Description:													**/
/**		This class primarily provides the game logic. All the			**/
/**		scores are set here along with the ability to reset them.		**/
/**		Also the two algorithms used to determine the suits and		**/
/**		values of each of the cards are also implemented here.			**/
/**		The deal method is also implemented here, this method			**/
/**		returns the image of the card plus sets a variable with the		**/
/**		card number - this allows the algorithms to determine the		**/
/**		suit / value of the card										**/
/*************************************************************************/

package pokerpkg;

import java.io.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import javax.swing.JMenuBar.*;
import java.awt.image.*;
import javax.imageio.ImageIO;
import java.util.*;

public class CardSet
{
	public static final int NO_OF_CARDS = 4;
	public static final int FULL_DECK = 52;
	public static final int FULL_SUIT = 13;

	private static int totalScore;
	private static int score;
	//This is an array of card objects
	private Card[] dealtCards;
	//Array stores the element numbers that were taken from the array of cards
	private int[] cardNumbers;
	private BufferedImage[] cardImagesForHand;
	private String winningHand;

	//The totalScore is initialsied in a static block
	//so that every time the user wants to start a new game the
	//the score wont be reset back to zero
	static{
		totalScore = 0;
	}

	public CardSet()
	{
		dealtCards = new Card[NO_OF_CARDS];
		cardNumbers = new int[NO_OF_CARDS];
		cardImagesForHand = new BufferedImage[NO_OF_CARDS];
		score = 0;

		for(int i=0; i<NO_OF_CARDS; i++)
			dealtCards[i] = new Card();
	}//end CardSet() constructor

	//Returns the score
	public int getScore()
	{
		return score;
	}//end getScore()

	//Returns the total score
	public int getTotalScore()
	{
		return totalScore;
	}//end getTotalScore()

	//Resets the score back to zero
	public void resetScore()
	{
		score = 0;
	}//end resetScore()

	//Resets the total score back to zero
	public void resetTotalScore()
	{
		totalScore = 0;
	}//end resetTotalScore()

	//Returns the string for the winning hand that gets displayed in the GUI
	public String getWinningHand()
	{
		return winningHand;
	}//end getWinningHand()

	//Returns an image of the card that was picked from the card object
	public BufferedImage getImagesOfPickedCards(int cardNumber)
	{
		return cardImagesForHand[cardNumber];
	}//end getImagesOfPickedCards()

	//Method resets scores back to zero
	public void clearScores()
	{
		totalScore = 0;
		score = 0;
	}//end clearScores()

	/*	The deal method returns an image of the card
		but also gets the card number and puts it into an array
		This array of card numbers is later used to determine the
		suits and values for each of the cards.
		The deal method is better called for each of the times
		you want to deal a card - i.e. this would be called four times
	*/
	public BufferedImage deal(int cardNumber)
	{
		//cardImagesForHand is an array of images that have been picked from the card objects
		cardImagesForHand[cardNumber] = dealtCards[cardNumber].pick();
		//the method getCardNumber simply returns the element number of the array of images
		//one is added to it so that the algorithms that determine the suit and value
		//can work a little better. Note: The dealtCards array is an array of Card objects
		cardNumbers[cardNumber] = (dealtCards[cardNumber].getCardNumber()+1);
		return cardImagesForHand[cardNumber];
	}//end deal()

	/*	Determines the suit type for each card
		This method works by cycling through the four card objects.
		Based on the card number which was determined at the deal method it
		checks what suit it is, for example the card number between one and
		four will be all aces. This algorithm is also dependent on how the sprite
		sheet was parsed. Note: the deal method must be called before
		this method in order for it to work
	*/
	public void determineSuit()
	{
		int var = 1;
		int range;
		String suitType = null;

		while(var<NO_OF_CARDS+1){
			range = var;

			if(var==1) suitType = "Club";
			if(var==2) suitType = "Diamond";
			if(var==3) suitType = "Heart";
			if(var==4) suitType = "Spade";

			for(int i=0; i<FULL_SUIT; i++){
				for(int j=0; j<NO_OF_CARDS; j++){
					if(range == cardNumbers[j])
						dealtCards[j].setCardSuit(suitType);
				}
				range+=NO_OF_CARDS;
			}
			var++;
		}
	}//end determineSuit()

	/*	Determines the value for each card
		This method works on the same principle as the determineSuit() method.
		As in this also works by cycling through the four card objects.
		Also by using the cardNumber variable which was determined at the deal method it
		checks what value it is, for example the card number between one and
		sixteen will be between one (ace) and 13 (king). This algorithm is also
		dependent on how the sprite sheet was parsed. Note: the deal method must
		be called before this method in order for it to work
	*/
	public void determineValue()
	{
		int value = 1;
		int lowRange = 1;
		int highRange = 4;

		for(int i=0; i<FULL_SUIT; i++){
			for(int j=0; j<NO_OF_CARDS; j++){
				if(cardNumbers[j] >= lowRange && cardNumbers[j] <= highRange)
					dealtCards[j].setCardValue(value);
			}
			value++;
			lowRange+=NO_OF_CARDS;
			highRange+=NO_OF_CARDS;
		}
	}//end determineValue()

	/*	The updateScore method simply checks each of the methods that see
		what hand the user has. If the user has a winning hand the score will be updated
		as appropiate.
	*/
	public void updateScore()
	{
		int count = 0;
		while(count!=1){
			if(checkRoyalFlush()){
				winningHand = "Royal Flush";
				score+=800;
				break;
			}
			else if(checkStraightFlush()){
				winningHand = "Straight Flush";
				score+=500;
				break;
			}
			else if(checkFourOfAKind()){
				winningHand = "Four of a Kind";
				score+=350;
				break;
			}
			else if(checkFlush()){
				winningHand = "Flush";
				score+=200;
				break;
			}
			else if(checkStraight()){
				winningHand = "Straight";
				score+=100;
				break;
			}
			else if(checkThreeOfAKind()){
				winningHand = "Three of a Kind";
				score+=75;
				break;
			}
			else if(checkTwoPair()){
				winningHand = "Two Pairs";
				score+=50;
				break;
			}
			else if(checkOnePair()){
				winningHand = "Pair";
				score+=10;
				break;
			}
			else winningHand = "You lose";
			count = 1;
		}
		totalScore+=score;
	}//end updateScore()

	/*Checks to see if the user has one pair*/
	public boolean checkOnePair()
	{
		int[] cardValue = new int[NO_OF_CARDS];
		for(int i=0; i<NO_OF_CARDS; i++)
			cardValue[i] = dealtCards[i].getCardValue();

		if(cardValue[0] == cardValue[1])
			return true;
		else if(cardValue[2] == cardValue[3])
			return true;
		else if(cardValue[1] == cardValue[2])
			return true;
		else if(cardValue[0] == cardValue[3])
			return true;
		else if(cardValue[0] == cardValue[2])
			return true;
		else if(cardValue[1] == cardValue[3])
			return true;
		else return false;
	}//end checkOnePair()

	/*Checks to see if the user has two pair*/
	public boolean checkTwoPair()
	{
		int[] cardValue = new int[NO_OF_CARDS];
		for(int i=0; i<NO_OF_CARDS; i++)
			cardValue[i] = dealtCards[i].getCardValue();

		if(cardValue[0] == cardValue[1] && cardValue[2] == cardValue[3])
			return true;
		else if(cardValue[1] == cardValue[2] && cardValue[0] == cardValue[3])
			return true;
		else return false;
	}//end checkTwoPair()

	/*Checks to see if the user has three of a kind*/
	public boolean checkThreeOfAKind()
	{
		int[] cardValue = new int[NO_OF_CARDS];
		for(int i=0; i<NO_OF_CARDS; i++)
			cardValue[i] = dealtCards[i].getCardValue();

		if(cardValue[0] == cardValue[1] && cardValue[1] == cardValue[2])
			return true;
		else if(cardValue[1] == cardValue[2] && cardValue[2] == cardValue[3])
			return true;
		else if(cardValue[0] == cardValue[2] && cardValue[2] == cardValue[3])
			return true;
		else if(cardValue[0] == cardValue[1] && cardValue[1] == cardValue[3])
			return true;
		else return false;
	}//end checkThreeOfAKind()

	/*Checks to see if the user has a straight*/
	public boolean checkStraight()
	{
		int[] cardValue = new int[NO_OF_CARDS];
		for(int i=0; i<NO_OF_CARDS; i++)
			cardValue[i] = dealtCards[i].getCardValue();

		//Insert Sort is used to sort the array in numeric order
		int temp, j, k= 0;

		for(j=1; j<NO_OF_CARDS; j++){
			temp = cardValue[j];
			k=j;
			while(k>0 && cardValue[k-1]>temp){
					cardValue[k] = cardValue[k-1];
					k--;
			}
			cardValue[k] = temp;
		}

		if( (cardValue[0] == (cardValue[1]-1))&&
			(cardValue[1] == (cardValue[2]-1))&&
			(cardValue[2] == (cardValue[3]-1)))
			return true;
		else return false;
	}//end checkStraight()

	/*Checks to see if the user has a flush*/
	public boolean checkFlush()
	{
		String[] cardSuit = new String[NO_OF_CARDS];
		for(int i=0; i<NO_OF_CARDS; i++)
			cardSuit[i] = dealtCards[i].getCardSuit();

		if( (cardSuit[0].equals(cardSuit[1]))&&
			(cardSuit[1].equals(cardSuit[2]))&&
			(cardSuit[2].equals(cardSuit[3])))
			return true;
		else return false;
	}//end checkFlush()

	/*Checks to see if the user has a four of a kind*/
	public boolean checkFourOfAKind()
	{
		int[] cardValue = new int[NO_OF_CARDS];
		for(int i=0; i<NO_OF_CARDS; i++)
			cardValue[i] = dealtCards[i].getCardValue();

		int count=0;
		for(int i=1; i<NO_OF_CARDS; i++){
			if(cardValue[0] == cardValue[i])
				count++;
		}
		if(count==3) return true;
		else return false;
	}//end checkFourOfAKind()

	/*Checks to see if the user has a straight flush*/
	public boolean checkStraightFlush()
	{
		if(checkStraight() && checkFlush())
			return true;
		else return false;
	}//end checkStraightFlush()

	/*Checks to see if the user has a royle flush*/
	public boolean checkRoyalFlush()
	{
		int check = 0;
		if(checkFlush()){
			int[] cardValue = new int[NO_OF_CARDS];
			for(int i=0; i<NO_OF_CARDS; i++)
				cardValue[i] = dealtCards[i].getCardValue();

			//Insert Sort
			int temp, j, k= 0;

			for(j=1; j<NO_OF_CARDS; j++){
				temp = cardValue[j];
				k=j;
				while(k>0 && cardValue[k-1]>temp){
						cardValue[k] = cardValue[k-1];
						k--;
				}
				cardValue[k] = temp;
			}
			if( (cardValue[0] == 1)&&
				(cardValue[1] == 11)&&
				(cardValue[2] == 12)&&
				(cardValue[2] == 13) )
				check = 1;
		}
		if(check == 1) return true;
		else return false;
	}//end checkRoyalFlush()
}//end CardSet class
