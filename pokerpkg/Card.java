/*************************************************************************/
/**	Name: Adam Drakeford												**/
/**	Student ID: C08306176												**/
/**	Class Description:													**/
/**		The main functionality of this class is to import the			**/
/**		images and place them into an array of size fifty two.			**/
/**		Then there is a method which randomly returns an image			**/
/**		from the array. There are other methods that sets and returns	**/
/**		the values and suits of each of the cards						**/
/*************************************************************************/

package pokerpkg;

import java.io.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import javax.swing.JMenuBar.*;
import java.awt.image.*;
import javax.imageio.ImageIO;
import java.util.*;

public class Card
{
	//Holds all 52 images of the cards
	static private BufferedImage[] cardsArray;
	//Holds the sprite sheet
	private BufferedImage cardsSpriteImage;
	private int cardNumber;
	private String cardSuit;
	private int cardValue;

	/*The constructor loads the image and stores the 52 images of the cards into the array*/
	public Card()
	{
		cardsArray = loadImage("images/cards.PNG");
	}//end Card() constructor

	/*	The load image method reads in the sprite sheat and splits
		it up into 52 images for the cards. It then places the
		images into an array of size 52 and type BufferedImage
	*/
	private BufferedImage[] loadImage(String fileName)
	{
		//This throws an exception back to main, it is incase the image cannot be
		//read or found
		try{
			cardsSpriteImage = ImageIO.read(new File(fileName));
		}catch (Exception e){
			System.out.print("Error");
		}

		//The size of each image and the number of rows and columns are defined here
		final int width = 79;
		final int height = 123;
		final int rows = 4;
		final int cols = 13;

		//Declares the size of the array - in this case it will be 52
		BufferedImage[] cards = new BufferedImage[rows*cols];

		/*	This is when it goes through the image and splits it up.
			This works by starting at the top left side of the image and
			working down
		*/
		for(int i=0; i<cols; i++){
			for(int j=0; j<rows; j++){
				cards[(i*rows)+j] = cardsSpriteImage.getSubimage(
					i*width,
					j*height,
					width,
					height
				);
			}
		}
		return cards;
	}//end LoadImage()

	/*This returns the array of images*/
	public BufferedImage[] getDeckOfCards()
	{
		return cardsArray;
	}//end getDeckOfCards()

	/*	This returns the element number of the cardsArray.
		It are these numbers that makes the algorithms that finds the
		suit and value of the cards work
	*/
	public int getCardNumber()
	{
		return cardNumber;
	}//end getCardNumber()

	/*	This is the pick method. It is this method that randomly picks
		a random number and returns the image from the element of that number
	*/
	public BufferedImage pick()
	{
		Random generator = new Random();
		int index;

		//This ensures that two cards of the same type dont get returned
		do{
			index = generator.nextInt(52);
			cardNumber = index;
		}while(cardsArray[index] == null);

		BufferedImage temp = cardsArray[index];
		cardsArray[index] = null;
		return temp;
	}//end pick()

	/*	This sets the suit of the card - the method determineSuit()
		in the CardSet class calls it
	*/
	public void setCardSuit(String cardSuit)
	{
		this.cardSuit = cardSuit;
	}//end setCardSuit()

	/*	This sets the value of the card - the method determineValue()
		in the CardSet class calls it
	*/
	public void setCardValue(int cardValue)
	{
		this.cardValue = cardValue;
	}//end setCardValue()

	/*This simply returns the suit of the card as a String*/
	public String getCardSuit()
	{
		return cardSuit;
	}//end getCardSuit()

	/*	This simply returns the value of the card as a integear value
		It is important to remember that values like "king" will not be
		returned, instead it will be 13
	*/
	public int getCardValue()
	{
		return cardValue;
	}//end getCardValue()
}//end Card class
